From: Andreas Barth <aba@not.so.argh.org>
To: adeodato@debian.org, ballombe@debian.org, luk@debian.org, he@debian.org
Cc: vorlon@debian.org
Subject: welcome to the release team
Date: Sun, 2 Jul 2006 10:09:49 +0200
Message-ID: <20060702080949.GT2818@mails.so.argh.org>
Content-Type: text/plain; charset=us-ascii
User-Agent: Mutt/1.5.9i

Hi,

welcome to the release team. Your should soon get the gid-bit of the
debian-release group, which in turn will grant you access to spohr (and
raff). As soon as you can login there, please touch a file in
/org/ftp.debian.org/testing/touchme/ with your login id, so that you can
get a hints-file.

Now, we're at the real stuff. Hints. They're in
/org/ftp.debian.org/testing/hints/. As soon as your file is moved there,
you could start adding them there. Some rules how we use these files
follow.

You have access to most commands, easy, hint, remove, block, unblock,
approve. easy and hint work on a group of packages, whereas all other
hints (that you have access to) work on individual packages. All hints
use the form package/version; in case of removals -package/version (not
at the remove hint, but if you want to easy -foo/1 bar/2); tpu packages
have IIRC tpu_package/version, also not at the approve hint.

Easy and hint both say "add these packages to testing" (and directly
fail if one of them isn't ready). Easy just does that, and commits if
the result isn't worse than what was before. Hint does however run a
full round of updating other packages before the comparison; this means,
it can handle more complex situations, but it also more time- and memory
consuming than easy, so use that one with care. For this reason, there
is also only a limited number of hint-hints allowed.

Remove removes a package from testing, block adds a block, unblock
overwrites *any* block, approve approves a package from tpu. Please let
the senior members of the release team take care of
approving/unblocking/removing important packages, i.e. anything >=
standard, etc. Also, please don't edit the freeze-file in the
hints-directory unless authorized to do so (whereas requests from Frans
Pop for the d-i-specific additions/deletions to that file are ok -
please be careful when we enter the next freeze steps as a package could
be frozen for multiple reasons). Something else you need to be careful
about is that for the non-group hints, any hint of the same kind and
same package can overwrite another hint for a different package version,
so please deactivate your hint if it is outdated (e.g.  a "unblock
foo/1"-hint could overwrite a "unblock foo/2"-one). About deactivating,
there is the finished-statement which stops britney from parsing any
lines further down, so please use that as a label for finishing hints
off. Please don't remove hints. You could also comment with a #-sign if
you need to turn a specific hint off (and for regular comments).

Please also tell in your hints file why you do something - this will
help all of us in understanding what happens. For removals, you should
not do them unless there is a RC-bug available -- and please quote the
bug number in your removal request, like
# 340538, 374160
remove apache2/2.0.55-4
(no, please don't try this one :).

Ok, what else? Britney runs usually short after dinstall; the results of
britney are committed to the database, so that "dak ls" will tell you the
new files already; however, as britney could be rerun during daytime,
don't disable hints before dinstall happened. Also, as spohr is quite
loaded, please be carefull with adding load. Britney and everything else
for testing lives in /org/ftp.debian.org/testing; the python/c-code in
the update_out directory. There are a couple of useful commands around
in ~aba/bin and ~vorlon/bin, like sls (dak ls, but also with sources), d
(diff package between unstable and testing), e2easy, e2remove, ...

I think that was most of it. If there are more questions, please don't
hesitate to ask us by mail or on IRC. As you have probably realized,
with more power comes also more responsibility. You are now leaving the
area where access rights protect every step, but where other people
trust you to not fiddle around at places you shouldn't. So, thank you
for volunteering for this task, and let's release etch together in time.


Cheers,
Your's Release Managers
