From: Steve Langasek <vorlon@debian.org>
To: debian-devel-announce@lists.debian.org
Subject: Release update

Hello world.

There's a steadily increasing buzz about the status of the sarge
release, now that the new installer is on the home stretch.  The release
team has been hard at work on finalizing a viable release schedule, but
it will take a little more time yet before that's ready to be announced.

In the meantime, we do need to announce the first stage of the sarge
freeze.

31 July		Hard-freeze of base+standard

With the release of debian-installer's second test candidate at the
beginning of August, we need to provide a fixed target for the d-i team.
Freezing base at this point will help us ensure the debian-installer
release can be used for shaking out its *own* bugs, instead of getting
caught up in new bugs from the base system.  Starting 31 July, no more
non-RC changes are allowed into testing for base packages or for
packages of priority standard and higher.  RC fixes are allowed in
through the testing-proposed-updates queue, and will be hand-approved by
the release team.  Base libraries in unstable must also NOT change their
shlibs, because these changes will not be propagated to testing before
the release!

Please note that for these purposes (and for the purposes of the ongoing
base dependency freeze), "base" packages are those packages which are
installed by debootstrap by the "sarge" target.  If you're not sure
whether this includes you, please check
<http://release.debian.org/base-packages.txt> for a list of binary
packages.

Also keep in mind that any RC bugs currently holding new versions of
base packages out of testing will need to be fixed in advance of 31
July, if you want to get your other changes into sarge.

Since long freezes become a problem, you can rest assured that once base
is frozen we will be moving to release as quickly as we can.  This means
that, even though there was no official BSP this weekend, it's still
open season on all RC bugs.  If you have some extra time, please
consider spending it on <http://bugs.debian.org/release-critical/>.  Of
particular importance now are the bugs listed at
<http://bugs.qa.debian.org/>.  There are still around 230 RC bugs
affecting sarge, which means around 230 package removals if these don't
get fixed.

Look for a full timeline to become available around the first of August.
The current thought is to release sarge by mid-September, but we're
trying not to promise anything we can't deliver.  We'll let you know
soon when we think this baby is due.

Cheers,
-- 
Steve Langasek                                     [vorlon@debian.org]
Debian Release Assistant, on behalf of the release team
