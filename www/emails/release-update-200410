Subject: Release update: please upload to unstable; toolchain; buildds; ...

Hello world,

After more than a month since the last update, it's about time that we
kept you informed about what's been happening release-wise.

First and foremost, one of the most frequently asked questions is still
about where packages should be uploaded.  Please upload to unstable,
unless there are reasons why the package in unstable is not suitable for
testing (such as earlier intrusive changes or shifting dependencies).
Even if a package is frozen, updates can still go through unstable.  See
below for details.

We've also added a few clarifications to the release policy, in response
to questions about the RC-ness of several reports: we do require that
packages build from source in the same distribution (i.e., sarge), and
broken Recommends are not RC, but should of course be fixed nevertheless
(like all bugs).

Additions to the release team: Andreas Barth <aba@not.so.argh.org> and
Frank Lichtenheld <djpig@debian.org> have been appointed as additional
Release Assistants to help the release managers with the sheer amount of
bugs, packages and other things to think of. Of course, this doesn't
mean you can stop taking care of your own RC bugs. ;)

One of our release blockers, the toolchain, seems to be sorted out at
last.  There is a new issue on mips*, but as this affects only R10000
machines which aren't supported, we'll probably defer that to etch; so
the toolchain is final with glibc 2.3.2.ds1-18 (already in testing).


KDE 3.3 will need to stay in unstable. We will release with KDE 3.2.


At this point, we do not have an ETA for the critical testing
autobuilders that are our key blockers for the release.  We can say that
the ftp-masters have been working on several bugs related to the
testing-security queue; while this is surely small comfort for those who
were hoping we would release before now, it does mean that we haven't
been at a complete standstill in spite of regrettable outward
appearances.

As before, while the autobuilder queues are being sorted out, there are
still a few outstanding items to keep the rest of us busy.

The installer team is moving towards a debian-installer release
candidate, which will include 2.4.27/2.6.8 kernels and various other
fixes. Testing on all architectures is always needed to help prepare
such d-i releases, in addition to testing once the builds are released.
A series of not-unprecedented last-minute bugs has kept the installer
team from finalizing the rc2 release[1]; at least one of the blocking
issues on Joey's list is known to still be outstanding, but should be
resolved within the next few days.  The installer team can always use
help in processing install reports as well.  Please contact
debian-boot@lists.debian.org if you're willing to help out.


The kernel team has announced[2] that it doesn't have the resources at
the moment to fix and maintain the i486 emulation patch that is
necessary to run sarge on a true i386 machine. There was some discussion
on whether the patch should be included in the kernel but not compiled
in by default, to give the users that need it the choice of just coping
with its security risks, but no final decision has been reached. Frank
Lichtenheld has tested that kernel, and done an upgrade with it.[3] The
kernel team will do more tests on Frank's machine.

There are upgrade problems due to glibc/kernel incompatibilities on
different platforms, including sparc32, hppa with 64bit kernel, arm and
mipsel. The problems with mips(el) and arm seem to be such that they
could be worked around during the upgrade (namely by installing a new
kernel first), so they qualify for the release notes. For sparc32, there
is no clean upgrade path from woody to sarge yet.  More investigation
is still needed for hppa64.

Anyone interested in helping with any of these upgrade issues should
coordinate with debian-kernel@lists.debian.org.


Last but not least, the count of release critical bugs affecting
sarge[4] is now in an acceptable range to really go for release.  The
original timeline would have us freezing with about 100 RC bugs left;
with 140-150 bugs left today, this puts us in a good position, but still
a long way from freezing (one big drawback was the identification of
undeclared file conflicts which led to a mass-filing of about 40 RC
bugs). There's plenty of room for patch submissions and NMUs here.
Removals for packages outside of base+standard that have open RC bugs
will be happening at an accelerated pace, so if you see RC bugs listed
on packages you use (or maintain, or depend on), now's the time to do
something about it. Any package with an open RC-bug is now poised for
removal.


Package uploads for sarge should continue to be made according to the
following guidelines.

  - If your package is frozen, but there are no changes in unstable that
    should not also be in testing, upload to unstable only and contact
    debian-release@lists.debian.org if changes need to be pushed through
    to testing.
  - If your package is frozen and the version in unstable includes
    changes that should NOT be part of sarge, contact
    debian-release@lists.debian.org with details about the changes you
    plan to upload (diff -u preferred) and, with approval, upload to
    testing-proposed-updates.  Changes should be limited to translation
    updates and fixes for important or RC bugs.
  - If your package depends on KDE and you have changes you need to make
    to fix important or RC bugs for sarge, you will need to upload to
    testing-proposed-updates as well, with the same requirements as for
    frozen packages.
  - All other package updates should be uploaded to unstable only.
    Please use urgency=high for uploads that fix RC bugs for sarge
    unless you believe there is a significant chance of breakage in the
    new package.



Finally, let's recap the original timeline, with the fixed dates traded
out for variables, to see where we're going once testing-security is
on-line...

  N+0 days
  ~140 RC bugs
  testing-proposed-updates, testing-security working for all
  architectures
  Official security support for sarge begins

The testing-proposed-updates queue is already in use for uploads of RC
bugfixes, but up to now testing-security is not in use.  Now that it's
ready, the security team can begin providing security support for sarge.
The sooner this can actually happen, the better.

With security support in place, adventurous users can begin testing the
upgrade path from woody to sarge.  Their feedback will be used for
further bugfixing of packages, and to start preparing release notes.


  N+5 days
  120 RC bugs
  Last call for low-urgency uploads

Although the original "last call" has come and gone, the doors are not
closed for further low-priority fixes to make there way into sarge until
after testing-security is up and running.

This allows us a period of time after the start of upgrade testing for
low-priority fixes to get uploaded if they're targeted for sarge.  Of
course, bugs that warrant uploads at a higher urgency can be fixed after
this date; including t-p-u uploads for RC bugs.


  N+16 days
  90 RC bugs
  d-i final
  Freeze time!

A release candidate of d-i will require about three weeks to prepare, so
preparations will need to begin before testing-security is fully
operational to meet this timeline. It's likely that d-i rc2 will be the
final release, and will itself be released well before this point.

If all goes well, for both RC bug fixes and the d-i release, we'll be
ready to freeze the rest of the archive at this point.  Changes to
base+standard are limited to RC fixes only; fixes for RC bugs and
translation updates are allowed for the rest of the archive via
testing-proposed-updates.


  N+30 days
  0 RC bugs

Any remaining release-critical bugs will be fixed through uploads to
testing-proposed-updates or by removals from sarge.

With a final cut of the installer in the bag, we will also be fixing any
remaining CD generation problems, as well as final tweaks to the
installation manual and release notes.  Before this point, we will also
need to have kernel upgrade paths sorted out for all architectures.

Around this time, we will be able to set a date for the full release.


Keep up the good work, folks.  We've solved some substantial problems,
and there are only a few major issues left.  We're still on the road to
release, even if at times it's two steps forward and one step back.  The
release team continues to be committed to finishing off sarge as quickly
as possible while maintaining the high level of quality our users
expect.

We will continue to send update emails out to you on a semi-regular basis.

Cheers,

-- 
Colin Watson                                       [cjwatson@debian.org]

[1] http://lists.debian.org/debian-boot/2004/11/msg00138.html
[2] http://lists.debian.org/debian-release/2004/10/msg00027.html
[3] http://lists.debian.org/debian-release/2004/10/msg00256.html
[4] http://bugs.debian.org/release-critical/
