##
# This file lists the current release goals for Lenny and additional candidates
# which may become a release goal in the future.
#
# Each release goal needs:
#  * One or two developers as advocate(s)
#  * Someone from the release team tracking the goal's status
#  * Short description explaining what needs to be done
#  * Short name used as a tag when user-tagging bug reports related to
#    this release goal
# The number of bugs and a way to prevent further problems of the same
# kind should also be listed.
##

# RELEASE GOALS
# =============

# full IPv6 support
  Advocate: Martin Zobel-Helas
  Description: Support IPv6 in all applications that support services over
   IPv4 at the moment.
  Bug-Tag: ipv6
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?which=tag&data=ipv6&archive=no
  State: confirmed (inherited from etch)
  
# double compilation support
  Advocate: Martin Zobel-Helas and Luk Claes
  Description: All packages should be able to be built twice in a
   row.
  Bug-User: debian-qa@lists.debian.org
  Bug-Tag: qa-doublebuild
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-qa@lists.debian.org&tag=qa-doublebuild
  State: confirmed

# No unmet recommends relations inside main
  Advocate: Luk Claes
  Description: Packages in main should be able to satisfy all recommend
   relations in main.
  Bug-User: debian-release@lists.debian.org
  Bug-Tag: goal-recommends
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=goal-recommends
  State: confirmed

# Drop debmake from Debian
  Advocate: Marc Brockschmidt and Andreas Barth
  Description: Update or remove all remaining packages that need debmake
   to build, then remove debmake.
  Bug-User: debian-release@lists.debian.org
  Bug-Tag: debmake
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=debmake
  State: finished

# I18n support in all debconf-using packages
  Advocate: Nicolas Francois and Thomas Huriaux
  Release-Team-Contact: Luk Claes
  Description: Packages using the Debian configuration management (debconf)
   must allow for translation of all messages displayed to the user by
   using a gettext-based system like po-debconf.
  Bug-User: debian-i18n@lists.debian.org
  Bug-Tag: not-using-po-debconf
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-i18n@lists.debian.org&tag=not-using-po-debconf
  State: finished

# full large file support (LFS)
  Advocate: ??
  Description: Fix all applications to remove arbitrary file size limits.
  Bug-Tag: lfs
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?which=tag&data=lfs&archive=no
  State: confirmed (inherited from etch)

# NFS version 4 support
  Advocate: Steinar H. Gunderson
  Description: Support version 4 of the NFS protocol everywhere.
  Bug-Tag: nfs-v4
  State: finished (inherited from etch)

# I18n support for package descriptions
  Advocate: Michael Bramer
  Release-Team-Contact: Luk Claes
  Description: Automate the update of description translations in the
   Debian archive.
   Update package management frontends to use the translated
   descriptions.
  Bug-Tag: ddtp
  State: confirmed

# UTF-8 debian/changelog and debian/control
  Advocate: Russ Allbery
  Release-Team-Contact: ??
  Description: Fix all remaining debian/changelog and debian/control
   files which don't use UTF-8. These are easily found by lintian via
   debian-changelog-file-uses-obsolete-national-encoding and
   debian-control-file-uses-obsolete-national-encoding.
  Bug-User: debian-release@lists.debian.org
  Bug-Tag: goal-utf8-control
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=goal-utf8-control
  State: finished

# Switch /bin/sh to dash
  Advocate: Petter Reinholdtsen?
  Release-Team-Contact: ??
  Description: Make sure dash is installed and preseed debconf question.
   Fix bashisms.
  Bug-User: debian-release@lists.debian.org
  Bug-Tag: goal-dash
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=goal-dash
  State: confirmed

# Prepare init.d-Scripts for dependency-based init systems
  Advocate: Petter Reinholdtsen
  Release-Team-Contact: ??
  Description: Fix remaining problems in the LSB headers in init scripts
   to allow later switching of the default init to something dependency-
   based.
  Bug-User: initscripts-ng-devel@lists.alioth.debian.org
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=initscripts-ng-devel@lists.alioth.debian.org
  State: confirmed

# piuparts-clean archive
  Advocate: Marc Brockschmidt and Luk Claes
  Description: All packages should pass the install stable version, upgrade
   to unstable version, purge piuparts test.
  State: confirmed
  Bug-User: debian-release@lists.debian.org
  Bug-Tag: piuparts-stable-upgrade
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=piuparts-stable-upgrade

# Support for future gcc versions
  Advocate: Martin Michlmayr
  Release-Team-Contact: Luk Claes
  Description: To reduce the amount of build failures early and to ease 
   transitions we would like to keep the archive in a state such that
   it builds with an upcoming GCC release and encourage developers to
   fix those build failures early.  Tests usually can be done with the
   gcc-snapshot package in unstable and GCC versions found in
   experimental.
  State: finished
  Bug-User: tbm@cyrius.com
  Bug-Tag: ftbfs-gcc-4.3
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=tbm@cyrius.com&tag=ftbfs-gcc-4.3

# Support for python2.5
  Advocate: Josselin Mouette <joss@debian.org>, Ari Pollak <aripollak@gmail.com>
  Release-Team-Contact: Marc Brockschmidt
  Description: Make all packages support python2.5 as standard python
   version.
  State: finished
  Bug-User: debian-release@lists.debian.org
  Bug-Tag: goal-python2.5
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=goal-python2.5

# Transition g77 -> gfortran
  Advocate: Kumar Appaiah, Matthias Klose, Riku Voipio
  Release-Team-Contact: Neil McGovern
  Description: Make packages build with gfortran as a replacement for g77
  Bug-User: debian-toolchain@lists.debian.org
  Bug-Tag: gfortran
  Bug-Url: http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-toolchain@lists.debian.org&tag=gfortran
  State: finished

# CANDIDATES
# ==========

# Dependency/File list predictability
  Advocate: Steinar H. Gunderson
  Release-Team-Contact: ??
  Description: Use the build chroot from hell to detect missing 
   --disable-foo and --without-foo flags and ensure that only specifically
   enabled features are built in.
  Bug-Tag: goal-enriched-chroot
  Known number: about 90 issues identified, probably around 200 issues in total
  State: pre-confirmed

# Set default system fonts depending on the system languages
  Advocate: Nicolas Spalinger
  Release-Team-Contact: Andreas Barth
  Description: Choose fonts to install for each language and devise
   a way to set the default font depending on the chosen language.
  Bug-Tag: i18n-fonts
  State: needs more info

# Enforce usage of dpkg substvars
  Advocate: Andreas Barth
  Description: Packages should always include ${shlibs:depends} in their 
   Depends line. If debhelper is used, they should also always use
   ${misc:depends}.
  Bug-Tag: missing-substvars
  State: preconfirmed

# Support of vm.overcommit_memory=2
  Advocate: Florian Weimer
  Release-Team-Contact: Andreas Barth
  Description: ???
  Bug-Tag: ???
  State: ???
