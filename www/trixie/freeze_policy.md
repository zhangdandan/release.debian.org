#! TITLE: trixie Freeze Timeline and Policy
#! SUBTITLE: let's release!


DRAFT
=====

Table of Contents
=================
* [Summary](#summary)
* [Before the freeze](#before)
* [Transition and Toolchain Freeze](#transition)
* [Soft Freeze](#soft)
* [Hard Freeze - for key packages and packages without autopkgtest](#hard)
* [Full Freeze](#full)
* [Appropriate changes during Hard and Full Freeze](#appropriate)
* [Removing packages from testing during the freeze](#removing)


<a name="summary"></a>Summary
=============================

The freeze for trixie will happen according to the following timeline:

 * TBA - Milestone 1 - Transition and Toolchain Freeze
 * TBA - Milestone 2 - Soft Freeze
 * TBA - Milestone 3 - Hard Freeze - for key packages and packages without autopkgtests
 * TBA - Milestone 4 - Full Freeze

These phases are fully explained below, but condense down to the following summary:

TODO we need some style definitions for this table

<table border=1>
<tr>
<th></th>
<th>Transition and Toolchain Freeze</th>
<th>Soft Freeze</th>
<th colspan=2>Hard Freeze</th>
<th>Full Freeze</th>
</tr>

<tr>
<td></td>
<td>TBA</td>
<td>TBA</td>
<td colspan=2>TBA</td>
<td>TBA</td>
</tr>

<tr>
<td></td>
<td></td>
<td>All packages</td>
<td>Non-key packages with autopkgtests</td>
<td>Key packages and packages without autopkgtests</td>
<td>All packages</td>
</tr>

<tr>
<td>Acceptable changes</td>
<td>No large/disruptive changes</td>
<td colspan=4>Only small, targeted fixes</td>
</tr>

<tr>
<td>Transitions</td>
<td colspan=5>No new transitions</td>
</tr>

<tr>
<td>Changes to toolchain packages</td>
<td colspan=5>Only after pre-approval</td>
</tr>

<tr>
<td>Min. migration delay</td>
<td>Standard (2/5/10d)</td>
<td>10 days</td>
<td>20 days</td>
<td>After unblock</td>
<td>After unblock</td>
</tr>

<tr>
<td>Autopkgtest bounty</td>
<td>Yes</td>
<td colspan=4>No</td>
</tr>

<tr>
<td>New src packages</td>
<td>Standard automatic migration</td>
<td colspan=4>No</td>
</tr>

<tr>
<td>Re-entry to testing</td>
<td>Standard automatic migration</td>
<td colspan=4>No</td>
</tr>

<tr>
<td>Adding/removing binary packages</td>
<td>Standard automatic migration</td>
<td colspan=4>No</td>
</tr>

<tr>
<td>Migration rules</td>
<td>Standard automatic migration</td>
<td colspan=2>Automatic migration, except if manually blocked</td>
<td>Manual review only</td>
<td>Manual review only</td>
</tr>

</table>

<a name="before"></a>Before the freeze
======================================


Plan your changes for trixie
----------------------------

If you are planning big or disruptive change, check the timeline to see if
it's still realistic to finish them before the Transition and Toolchain
Freeze. Keep in mind
that you might need cooperation from other volunteers, who might not
necessarily have the time or capacity to work on this. You can stage your
changes in experimental to make sure all affected packages are ready before
uploading the changes to unstable.


Fix release-critical bugs
-------------------------

You can help the development of trixie by fixing RC bugs, before and during
the freeze. An overview of bugs can be found on
<a href="https://udd.debian.org/bugs/">the UDD bugs page</a>.

You can also join the #debian-bugs irc channel on irc.oftc or join a
<a href="https://wiki.debian.org/BSP">Bug Squashing Party</a>.


Add non-trivial autopkgtests to your package
--------------------------------------------

Autopkgtests help to discover regressions in your package or its dependencies,
before they can migrate to testing. Please consider adding autopkgtests to
your package that test a substantial part of its functionality.

Please note that all rules in the freeze policy about 'autopkgtests' only
apply to non-trivial autopkgtests. If you think it's useful to add
autopkgtests that only do limited testing of your package, please mark them as
superficial (see the
<a href="https://salsa.debian.org/ci-team/autopkgtest/raw/master/doc/README.package-tests.rst">autopkgtest
specification</a>).


Testing migration
-----------------

The 'standard' testing migration rules apply:

 * automatic migration after 2/5/10 days for priority high/medium/low packages
 * faster migration for packages with successful autopkgtests
 * no migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * migration for packages only allowed if all their binary packages have been built on buildds
 * auto-removal of non-key packages
 * packages not in testing can migrate to testing
 * no manual review by the release team


<a name="transition"></a>Transition and Toolchain Freeze
=================================================================

Starting XX, the main changes to what is expected from maintainers are:

 * no new transitions
 * no changes to packages that are part of the toolchain
 * no large/disruptive changes

No new transitions
------------------

When changes in a package cause the need for changes in other packages, we
call this a 'transition'. The most common example is a library transition.
Transitions require coordination between maintainers and can take a long time
to finish, especially when bugs are discovered during the transition.
Unfinished transitions can block the migration of unrelated fixes in the
packages involved in the transition. From the start of the Transition and
Toolchain Freeze, it's no longer appropriate to start new transitions.


No changes to packages that are part of the toolchain
------------------------------------------------------------------------

Debian has quite some packages that influence the content of packages
and how they are built, i.e. most packages that are part of
(build-)essential and other toolchain packages. The impact of bugs in
these packages can take a lot of time to resolve because it requires
figuring out which packages are affected and how they need to be
rebuild after the bug itself is fixed. Therefore, changes to these
kind of packages are no longer appropriate.

Because this description is unfortunately slightly vague, the packages
for which this policy applies are listed <a
href="essential-and-build-essential.txt">explicitly</a>. As the list
is manually put together, it's possible that packages are missing, so
if in doubt, contact the Release Team.

Of course we want to see known RC bugs fixed before we release. If you
think changes are needed, please coordinate with the release team
before uploading to unstable. Consider staging changes in
experimental.

No large/disruptive changes
---------------------------

Any change which is large or disruptive, which requires coordination or has a
higher change of regressions is no longer appropriate at this time.


Be careful with new upstream releases
-------------------------------------

Before uploading a new upstream release of your package, double-check if the
changes are appropriate at this time. If you're unsure, it might be better to
upload to experimental instead.


Do fix RC bugs
--------------

If a package still has RC bugs at the beginning of the Transition and Toolchain
Freeze,
please try to fix them. Note that you should do that in a targeted way,
respecting the rules above.


When in doubt, talk to the release team
---------------------------------------

The rules above are not enforced in an automated way. We ask all maintainers
to follow these rules when uploading packages. If you're unsure about a change
you want to do, don't hesitate to talk to the release team. The recommended way
is via an unblock bug in the bts which has the regular unblock tags and an
appropriate title, e.g. starting with `pre-approval`.


Testing migration
-----------------

Changes in the rules for automatic testing migration:

 * None.

As the rules above are not automatically enforced, the 'standard' testing
migration rules are enforced.


<a name="soft"></a>Soft Freeze
==============================

Starting XX, only **small, targeted fixes** are appropriate for
trixie. We want maintainers to focus on small, targeted fixes. This is
mainly at the maintainers discretion, there will be no hard rule that will be
enforced.

Please note that new transitions, new versions of packages that are part of
(build-)essential or large/disruptive changes remain inappropriate.

The release team might block the migration to testing of certain changes if
they might cause disruption for the release process.

Increased delay for all testing migrations
------------------------------------------

The testing migration delay of all packages will be increased to 10 days. This
means the rate of changes to testing will slow down. This will also increase
the chance that regressions are discovered before they reach testing.

The minimum delay of 10 days will also apply to packages with successful
autopkgtests.


No new packages and no re-entry to testing
------------------------------------------

Packages that are not in testing will not be allowed to migrate to testing.
This applies to new packages as well as to packages that were removed from
testing (either manually or by auto-removals). Packages that are not in trixie
at the start of the soft freeze will not be in the release.

Dropping or adding binary packages to a source package, moving binaries
between source packages or renaming source or binary packages is no longer
allowed. Packages with these changes will not be allowed to migrate to
testing. These changes are also no longer appropriate in unstable.

Please note that packages that are in trixie at the start of the soft freeze
can still be removed if they are buggy. This can happen manually or by the
auto-removals. Once packages are removed, they will not be allowed to come
back.


No changes in unstable that are not targeted for trixie
---------------------------------------------------------

Don't upload changes to unstable that are not targeted for trixie. Having
changes in unstable that are not targeted/appropriate for trixie could
complicate fixes for your package and related packages (like dependencies and
reverse dependencies).


Testing migration
-----------------

Changes in the rules for testing migration:

 * manual review by the release team for some packages
 * migration delay always at least 10 days
 * no faster migration for packages with successful autopkgtests
 * packages not in testing can not migrate (back) to testing

The following rules still apply:

 * no migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * migration for packages only allowed if all their binary packages have been built on buildds
 * auto-removal of non-key packages


<a name="hard"></a>Hard Freeze - for key packages and packages without autopkgtest
==================================================================================

Starting XX, key packages and packages without autopkgtest will be
treated as in the Full Freeze (see below) while non-key packages with
autopkgtest will be treated as during the Soft Freeze (see above).

See below for a detailed description of the changes that are allowed and the
procedure to request an unblock.


Hard freeze for key packages
----------------------------

As <a href="https://udd.debian.org/cgi-bin/key_packages.yaml.cgi">key
packages</a> are exempted from auto-removals, RC bugs or regressions in key
packages can potentially cause delays in the freeze process. To decrease the
change of new regressions in testing, all changes to key packages will need an
unblock by the release team.


Hard freeze for non-key packages without autopkgtests
-----------------------------------------------------

Changes to non-key packages that don't have autopkgtests require an unblock by
the release team.


Soft freeze for non-key packages with autopkgtests
--------------------------------------------------

For non-key packages with (non-trivial) autopkgtests, the rules of the soft
freeze still apply, but the migration delay is increased to 20 days. This
allows targeted fixes to your package to reach testing (eventually), without
the need for the release team to review them.

Please note that for these packages, the rules from the soft freeze will still
not be automatically enforced. However, your change could cause RC bugs or
regressions that might result in the (auto-)removal of your package and its
reverse dependencies. If your upload clearly violates the rules above, and
causes regressions in testing, it might be manually removed without warning.

If you upload contains a targeted fix that is urgent (security update, ...),
you can file an unblock request to ask for faster migration to testing.

Testing migration
-----------------

Changes in the rules for testing migration:

 * manual review by the release team for
   * key packages
   * non-key packages without autopkgtests
 * non-key packages with autopkgtests: automatic migration after 20 days

The following rules still apply:

 * migration delay always at least 10 days
 * no faster migration for packages with successful autopkgtests
 * no migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * migration for packages only allowed if all their binary packages have been built on buildds
 * auto-removal of non-key packages
 * packages not in testing can not migrate (back) to testing


<a name="full"></a>Full Freeze
==============================

We only intend to start the Full Freeze shortly before the actual release of
trixie. The starting date of the Full Freeze will be decided when we start
planning a release date and will be announced at least 14 days before it
comes into effect. During the Full Freeze, all packages can only migrate to
testing after manual review by the release team. For key packages and packages
without autopkgtests, the rules stay the same. From the start of the full
freeze, those rules will also apply to other packages.

We have some criteria for what changes we are going to accept. These are
listed below. These criteria will become more rigid as the freeze progresses.

The release managers may make exceptions to these guidelines as they
see fit. **Such exceptions are not precedents and you should not
assume that your package has a similar exception.** Please talk to us
if you need guidance, but please consult the
<a href="https://release.debian.org/trixie/FAQ.html">FAQ</a> first.

Please talk to us early and do not leave issues to the last minute. We
are happy to advise in case you need the release team's help to fix RC
bugs (e.g. to remove an old package)


Testing migration
-----------------

Changes in the rules for testing migration:

 * manual review by the release team for all packages, based on the criteria
   listed below

The following rules continue to apply:

 * no migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * auto-removal of non-key packages
 * packages not in testing can not migrate to testing


<a name="appropriate"></a>Appropriate changes during Hard and Full Freeze
=========================================================================

 1. targeted fixes for release critical bugs (i.e., bugs of severity critical, grave, and serious);
 1. fixes for severity: important bugs, only when this can be done via unstable;
 1. translation updates and documentation fixes, only when this can be done via unstable;
 1. updates to packages directly related to the release process (i.e. with
	references to the current layout of the archive), only when this can be done via unstable;

Note that when considering a request for an unblock, the changes
between the (proposed) new version of the package in `unstable` and
the version currently in `testing` are taken in to account. If there
is already a delta between the package in `unstable` and `testing`,
the relevant changes are all of those between `testing` and the new
package, not just the incremental changes from the previous `unstable`
upload. This is also the case for changes that were already in
`unstable` at the time of the freeze, but didn't migrate at that
point. If unstable has changes that are not appropriate for testing at this
point of the freeze, please revert them, even if that means reverting to an
earlier upstream version.

We very strongly prefer changes that can be done via unstable instead of
testing-proposed-updates. If there are unrelated changes in unstable, we ask
you to revert these changes instead of making an upload to
testing-proposed-updates. Hence, and also because it may impact other packages
in unstable that try to migrate to trixie, it is recommended that, during the
freeze, you do not upload to unstable any changes for which you do not intend
to request an unblock.


Applying for an unblock
-----------------------
 1. Prepare a **source** `debdiff` between the version in `testing` and `unstable`
 1. Look at the entire resulting debdiff **before uploading to unstable**. If the debdiff it big, or contains changes that are unrelated to the fixes you want, don't upload the package.
// TODO list a maximum number of lines?
 1. Use `reportbug` to report an unblock bug against the `release.debian.org` meta-package. Attach the source diff. Include a detailed justification of the changes and references to bug numbers.
 1. Please include a concise description of the issue you are trying to fix.  Explain how you verified that the changes actually fix the issue. If you have reports of tests that confirm the fix works, please include pointer to them
 1. If the diff is small and you believe it will be approved, you can upload it to unstable before filing the unblock request to avoid a round-trip.
 1. Depending on the queue, there may be some delay before you receive further instructions.

If you are unable to bring your fix through unstable, for example because your
package in unstable is dependent on a version of another package that isn't
available in trixie and that is not being unblocked, the release team can
grant you permission to use the `testing-proposed-updates` mechanism. Make sure
you fix the issue in unstable following the rules outlined above and prepare a
no-change upload targeting `testing-proposed-updates` but **do not upload it**,
and then contact us through an unblock bug. testing-proposed-updates is not
meant to prevent "ugly" version numbers for packages that already have a newer
upstream version in unstable. You're requested to revert the upstream version
instead, e.g. by using a `new-version`+really`old-version` versioning scheme.

Targeted fixes
--------------

A targeted fix is one with only the minimum necessary changes to
resolve a bug. The freeze process is designed to make as few changes
as possible to the forthcoming release. Uploading unrelated changes is
likely to result in a request for you to revert them if you want an
unblock.

In most cases, it's not appropriate to upload a new upstream release at this
point. New upstream release usually contain unrelated changes, which might be
inappropriate or make review much more difficult. Uploading a new upstream
release is only appropriate when the resulting debdiff doesn't contain changes
that wouldn't be in the debdiff of a targeted change. When in doubt, ask for
pre-approval before uploading a new upstream release.


Some examples of changes that are undesirable during a freeze:

 1. bumping the debhelper compat level
 1. switching to a different packaging helper
 1. adding or dropping a systemd unit or an init script
 1. adding, removing or renaming binary packages
 1. adding or removing support for a language (version)
 1. moving files between binary packages
 1. changing relations (depends, conflicts, ...) between packages
 1. changes that affect other packages
 1. dropping a -dbg package in favour of -dbgsym
 1. rearranging code, 'cleanups', etc


<a name="removing"></a>Removing packages from testing during the freeze
=======================================================================

Throughout the freeze, we will continue to remove non-key
packages with RC bugs in testing and their reverse dependencies automatically.
As usual, the auto-removal system will send a notice before this happens.
Please note that it is not enough for the bug to be fixed in unstable.
The fix (in unstable or testing-proposed-updates) must be done in such a way that
the fixed package can be unblocked and allowed to migrate to testing, based on the rules
listed above. **You must contact us to ensure the fix is unblocked - do not rely on
the release team to notice on their own.**

Manual removal may still happen without warning before the standard auto-removal
periods have passed, when the package is blocking other fixes.

After the soft freeze begins, removed packages will **not** be permitted to re-enter testing.

